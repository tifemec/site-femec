# Site Femec Koinonia


# Ambiente de Desenvolvimento

1. Abrir o Prompt
2. Entrar na pasta escolhida
3. Clone o repositório.
4. Entrar na pasta clonada
5. Copia o projeto da Femec para essa pasta
6. Vê o status dos arquivos: (untracked)
7. Adiciona os arquivos que serão commitados em breve
8. Ve a mudança do status dos arquivos (unmodified ou modified)
9. Commitar os arquivos que já foram adicionados
10. Ve a mudança no status dos arquivos
11. Checar se o commit funcionou
12. Checar o repositório remoto
13. Subir os arquivos commitados para o repositório online


## Instalação
### Clone o repositório (altere a parte do link abaixo com o link da aba Visão Geral do repositório) e entre na pasta

```console
git clone https://annekelly13@bitbucket.org/tifemec/site-femec.git
cd app-femec
```

### Copia os arquivos do Site da Femec para essa pasta
### Checa se de fato copiou os arquivos

Unix
```console
ls
```
Windows
```console
dir
```

### Vê o status dos arquivos: (untracked provavelmente ou modified)

```console
git status
```

### Adiciona os arquivos que serão enviados ao repositório em breve
```console
git add .        #se for adicionar todos os arquivos modificados
	
#OU
	
git add caminho/nomearquivo.extensao         #se for adicionar apenas algum arquivo especifico dentre os modificados
```

### Vê a mudança dos status dos arquivos (unmodified ou modified)

```console
git status
```

### Commitar os arquivos que já foram adicionados
```console
git commit -m "Descricao do que está subindo no repositorio"
	
	#OU em caso de commitar so algum dos arquivos ja adicionados
	
git commit caminho/nomearquivo.extensao -m "Descricao do que está subindo no repositorio" 
```

### Vê a mudança dos status dos arquivos (eles vão sumir)

```console
git status
```

### Checar se o commit funcionou
```console
git log
```

### Checar o repositório remoto (o padrao se chama origin e aponta para o link do seu repositorio já)
```console
git remote -v
```

### Subir os arquivos commitados para o repositório online
```console
git push origin master
```